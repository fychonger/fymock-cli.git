# fymock-cli

#### 介绍
vue项目无侵入引入mockjs的简易cli工具

#### 软件架构
软件架构说明

#### 安装教程

##### step1

进入到`vue`项目的根目录

##### step2

```javascript
npm i fymock -g
```



```javascript
fymock init
```

###### or

```
npx fymock init
```

##### step3

```javascript
.env.development文件
 # mock status
 MOCK_STATUS = true
 MOCK_TIMEOUT = 200
```

##### step4

```javascript
vue.config.js文件
module.exports = {
    devServer: {
        before: process.env.MOCK_STATUS === 'true' && require('./mock/index')
    }
}
```

大功告成，运行项目即可~

#### 使用说明

##### 1.mockjs模板数据

在vue项目根目录下自动生成的mock文件夹中，有个modules文件夹，里面预先放置login.json5、user.json5两个模块的mock数据(ps:如不喜欢可以删除重建)

```javascript
{
    // H5 登录
    'login/h5': {
       'user_id|+1': 1, // 用户id
       name: '@cname', // 用户昵称
       avatar: "@image('200x100', '#894FC4', '#FFF', 'png', '@cname')", // 用户头像
       login_time: '@datetime', // 用户登录时间
    }
    // ...
      // 这里可以添加更多的接口模板
      // 键名对应接口名
      // 键值对应mock模板
}
```

其中'login/h5'为接口名，其后为对应的mock模板，**模板数据修改保存会激活热重载，无需重启vue服务**

> vscode安装json5插件，会添加对.json5格式文件的高亮显示

##### 2.调用

```javascript
xxx.vue
 axios.post('/api/login/h5').then(res=>{
     console.log(res) // 获取到的数据
 }).catch(error => console.log(error))
```

其中‘/api/login/h5’为标识符‘api’与modules文件夹中对应的模板键名‘login/h5’的拼接

> 目前所有接口默认为post请求

##### 3.延迟

目前默认延迟响应时间为200ms，可设置`.env.development`文件中`MOCK_TIMEOUT`进行更改(一般情况下，不回去变更)，更改完成需要重启vue项目

##### 4.关闭mock

可设置`.env.development`文件中`MOCK_STATUS=false`进行关闭mock，更改完成需要重启vue项目

