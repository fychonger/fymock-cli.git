// 将同步转化为promise
const {promisify} = require('util')
// 字体工具
const figlet = promisify(require('figlet'))

const clear = require('clear')
const {log} = require('./log')


const {clone} = require('./download')

module.exports = async name => {
    // 打印欢迎界面
    clear()
    const data = await figlet(`FY  WITH  U`)
    log(data)

    // clone
    log(`初始化ing...`)
    await clone('https://gitee.com/fychonger/mock-repo.git', 'mock')

}