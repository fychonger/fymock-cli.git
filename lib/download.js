const { promisify } = require('util')
const shell = require('shelljs')
const {log} = require('./log')
// 下载git项目并删除.git文件
module.exports.clone = async function (repo, desc) {
    const download = promisify(require('git-clone'))
    const ora = require('ora')

    const process = ora(`下载.....${repo}`)
    process.start()
    await download(repo, desc, () => {
        process.succeed()
        // 删除.git文件
        log(`删除.git文件ing...`)

        shell.rm('-rf', `./mock/.git`)

        // 安装依赖
        log('安装依赖ing...')
        shell.exec('npm install chalk chokidar json5 mockjs -D')
        process.succeed()
        log(`
        To get Start:
        =====================================
        
            .env.development
                // ..
                # mock status
                MOCK_STATUS = true
                MOCK_TIMEOUT = 200
        
        
            vue.config.js
                // ..
                devServer: {
                    before: process.env.MOCK_STATUS === 'true' && require('./mock/index')
                }
        
        =====================================
                `)
    })
}
