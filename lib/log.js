const chalk = require('chalk')
module.exports.log = (content) => {
    console.log(chalk.green(content))
}
